extends Node

signal fall_damage(vertical_velocity)

# TODO: figure out how to split this into ECS
@export var SPEED = 5.0
@export var JUMP_VELOCITY = 4.5
@export var ROTATION_SPEED = 1.0
@export var check_for_fall_damage = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var parent_node
var movement_vector = Vector2()
var target_node
var landed = false
var max_y_velocity = 0

# TODO: figure out how to save cpu cycles by having movement code in an if block

func _ready():
	parent_node = get_parent()

func move_towards_node(node):
	var node_position = node.get_position()
	parent_node.look_at(Vector3(node_position.x,parent_node.get_position().y,node_position.z))
	return parent_node.get_position().direction_to(node_position)

func _physics_process(delta):
	
	if check_for_fall_damage:
		match parent_node.get_slide_collision_count():
			0:
				if landed:
					landed = false
				# > because we are comparing to -velocity, which we will abs() later
				if max_y_velocity > parent_node.velocity.y:
					max_y_velocity = parent_node.velocity.y
			1:
				if not landed:
					landed = true
					fall_damage.emit(abs(max_y_velocity))
					max_y_velocity = 0
	
	var direction
	if target_node:
		direction = move_towards_node(target_node)
	
	#TODO: split gravity into it's own component
	# Add the gravity.
	if not parent_node.is_on_floor():
		parent_node.velocity.y -= gravity * delta

	if not direction:
		direction = (parent_node.transform.basis * Vector3(movement_vector.x, 0, movement_vector.y)).normalized()
	if direction:
		parent_node.velocity.x = direction.x * SPEED
		parent_node.velocity.z = direction.z * SPEED
	else:
		parent_node.velocity.x = move_toward(parent_node.velocity.x, 0, SPEED)
		parent_node.velocity.z = move_toward(parent_node.velocity.z, 0, SPEED)

	parent_node.move_and_slide()
